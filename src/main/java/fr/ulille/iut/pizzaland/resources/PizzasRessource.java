package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDTO;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Path("/pizzas")
public class PizzasRessource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private PizzaDao pizzas;

	@Context
	public UriInfo uriInfo;

	public PizzasRessource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createAssociationTable();

	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzasResource:getAll");
		List<PizzaDto> p = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		return p;

	}

	@GET
	@Path("{id}")
	public PizzaDto getOnePizza(@PathParam("id") long id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);
			return Pizza.toDto(pizza);
		}
		catch ( Exception e ) {
			// Cette exception générera une réponse avec une erreur 404
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@POST
	public Response createPizza(PizzaCreateDTO pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if ( existing != null ) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
			long id = pizzas.insert(pizza.getName());
			pizza.setId(id);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

			return Response.created(uri).entity(pizzaDto).build();
		}
		catch ( Exception e ) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") long id) {
		if (pizzas.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") long id) {
		Pizza pizza = pizzas.findById(id);
		if ( pizza == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return pizza.getName();
	}	
	
	//non fonctionnel
	@GET
	@Path("{id}/ingredients")
	public String getPizzaIngredients(@PathParam("id") long id) {
		Pizza pizza = pizzas.findById(id);
		if ( pizza == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		PizzaDto dto = Pizza.toDto(pizza);
		return dto.getContenus();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name) {
		Pizza existing = pizzas.findByName(name);
		if ( existing != null ) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = new Pizza();
			pizza.setName(name);

			long id = pizzas.insert(pizza.getName());
			pizza.setId(id);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

			return Response.created(uri).entity(pizzaDto).build();
		}
		catch ( Exception e ) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}


}
